<?php
/**
 * @file
 * cash_flow_income.features.field.inc
 */

/**
 * Implements hook_field_default_fields().
 */
function cash_flow_income_field_default_fields() {
  $fields = array();

  // Exported field: 'comment-comment_node_income-comment_body'.
  $fields['comment-comment_node_income-comment_body'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '1',
      'deleted' => '0',
      'entity_types' => array(
        0 => 'comment',
      ),
      'field_name' => 'comment_body',
      'foreign keys' => array(
        'format' => array(
          'columns' => array(
            'format' => 'format',
          ),
          'table' => 'filter_format',
        ),
      ),
      'indexes' => array(
        'format' => array(
          0 => 'format',
        ),
      ),
      'locked' => '0',
      'module' => 'text',
      'settings' => array(),
      'translatable' => '0',
      'type' => 'text_long',
    ),
    'field_instance' => array(
      'bundle' => 'comment_node_income',
      'default_value' => NULL,
      'deleted' => '0',
      'description' => '',
      'display' => array(
        'default' => array(
          'label' => 'hidden',
          'module' => 'text',
          'settings' => array(),
          'type' => 'text_default',
          'weight' => 0,
        ),
      ),
      'entity_type' => 'comment',
      'field_name' => 'comment_body',
      'label' => 'Comment',
      'required' => TRUE,
      'settings' => array(
        'text_processing' => 1,
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'module' => 'text',
        'settings' => array(
          'rows' => 5,
        ),
        'type' => 'text_textarea',
        'weight' => 0,
      ),
    ),
  );

  // Exported field: 'node-income-body'.
  $fields['node-income-body'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '1',
      'deleted' => '0',
      'entity_types' => array(
        0 => 'node',
      ),
      'field_name' => 'body',
      'foreign keys' => array(
        'format' => array(
          'columns' => array(
            'format' => 'format',
          ),
          'table' => 'filter_format',
        ),
      ),
      'indexes' => array(
        'format' => array(
          0 => 'format',
        ),
      ),
      'locked' => '0',
      'module' => 'text',
      'settings' => array(),
      'translatable' => '0',
      'type' => 'text_with_summary',
    ),
    'field_instance' => array(
      'bundle' => 'income',
      'default_value' => NULL,
      'deleted' => '0',
      'description' => 'Enter anything that needs to be known about this line item.',
      'display' => array(
        'default' => array(
          'label' => 'hidden',
          'module' => 'text',
          'settings' => array(),
          'type' => 'text_default',
          'weight' => '7',
        ),
        'teaser' => array(
          'label' => 'hidden',
          'module' => 'text',
          'settings' => array(
            'trim_length' => 600,
          ),
          'type' => 'text_summary_or_trimmed',
          'weight' => 0,
        ),
      ),
      'entity_type' => 'node',
      'field_name' => 'body',
      'label' => 'Notes',
      'required' => 0,
      'settings' => array(
        'display_summary' => 0,
        'text_processing' => '1',
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'text',
        'settings' => array(
          'rows' => '5',
          'summary_rows' => 5,
        ),
        'type' => 'text_textarea_with_summary',
        'weight' => '8',
      ),
    ),
  );

  // Exported field: 'node-income-field_amount'.
  $fields['node-income-field_amount'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '1',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'field_amount',
      'foreign keys' => array(),
      'indexes' => array(),
      'locked' => '0',
      'module' => 'number',
      'settings' => array(
        'decimal_separator' => '.',
        'precision' => 10,
        'scale' => 2,
      ),
      'translatable' => '0',
      'type' => 'number_decimal',
    ),
    'field_instance' => array(
      'bundle' => 'income',
      'default_value' => NULL,
      'deleted' => '0',
      'description' => 'Enter the amount of income.',
      'display' => array(
        'default' => array(
          'label' => 'above',
          'module' => 'editablefields',
          'settings' => array(
            'click_to_edit' => 0,
            'empty_text' => '',
            'fallback_format' => 'number_decimal',
            'fallback_settings' => array(
              'decimal_separator' => '.',
              'prefix_suffix' => 1,
              'scale' => '2',
              'thousand_separator' => ',',
            ),
          ),
          'type' => 'editable',
          'weight' => '1',
        ),
      ),
      'entity_type' => 'node',
      'field_name' => 'field_amount',
      'label' => 'Amount',
      'required' => 1,
      'settings' => array(
        'max' => '',
        'min' => '0.01',
        'prefix' => '',
        'suffix' => '',
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 0,
        'module' => 'number',
        'settings' => array(),
        'type' => 'number',
        'weight' => '2',
      ),
    ),
  );

  // Exported field: 'node-income-field_amount_transferred'.
  $fields['node-income-field_amount_transferred'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '1',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'field_amount_transferred',
      'foreign keys' => array(),
      'indexes' => array(),
      'locked' => '0',
      'module' => 'number',
      'settings' => array(
        'decimal_separator' => '.',
        'precision' => 10,
        'scale' => 2,
      ),
      'translatable' => '0',
      'type' => 'number_decimal',
    ),
    'field_instance' => array(
      'bundle' => 'income',
      'default_value' => NULL,
      'deleted' => '0',
      'description' => 'Enter the amount transferred and usable for payments.',
      'display' => array(
        'default' => array(
          'label' => 'above',
          'module' => 'editablefields',
          'settings' => array(
            'click_to_edit' => 0,
            'empty_text' => '',
            'fallback_format' => 'number_decimal',
            'fallback_settings' => array(
              'decimal_separator' => '.',
              'prefix_suffix' => 1,
              'scale' => '2',
              'thousand_separator' => ',',
            ),
          ),
          'type' => 'editable',
          'weight' => '9',
        ),
      ),
      'entity_type' => 'node',
      'field_name' => 'field_amount_transferred',
      'label' => 'Amount transferred',
      'required' => 0,
      'settings' => array(
        'max' => '',
        'min' => '0',
        'prefix' => '',
        'suffix' => '',
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 0,
        'module' => 'number',
        'settings' => array(),
        'type' => 'number',
        'weight' => '5',
      ),
    ),
  );

  // Exported field: 'node-income-field_business_related'.
  $fields['node-income-field_business_related'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '1',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'field_business_related',
      'foreign keys' => array(),
      'indexes' => array(
        'value' => array(
          0 => 'value',
        ),
      ),
      'locked' => '0',
      'module' => 'list',
      'settings' => array(
        'allowed_values' => array(
          0 => 'No',
          1 => 'Yes',
        ),
        'allowed_values_function' => '',
      ),
      'translatable' => '0',
      'type' => 'list_boolean',
    ),
    'field_instance' => array(
      'bundle' => 'income',
      'default_value' => array(
        0 => array(
          'value' => 1,
        ),
      ),
      'deleted' => '0',
      'description' => 'If this <em>income</em> came from business-related activities, check this box.',
      'display' => array(
        'default' => array(
          'label' => 'above',
          'module' => 'list',
          'settings' => array(),
          'type' => 'list_default',
          'weight' => '6',
        ),
      ),
      'entity_type' => 'node',
      'field_name' => 'field_business_related',
      'label' => 'Business-related',
      'required' => 0,
      'settings' => array(
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'options',
        'settings' => array(
          'display_label' => 1,
        ),
        'type' => 'options_onoff',
        'weight' => '6',
      ),
    ),
  );

  // Exported field: 'node-income-field_destination_account'.
  $fields['node-income-field_destination_account'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '1',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'field_destination_account',
      'foreign keys' => array(
        'tid' => array(
          'columns' => array(
            'tid' => 'tid',
          ),
          'table' => 'taxonomy_term_data',
        ),
      ),
      'indexes' => array(
        'tid' => array(
          0 => 'tid',
        ),
      ),
      'locked' => '0',
      'module' => 'taxonomy',
      'settings' => array(
        'allowed_values' => array(
          0 => array(
            'vocabulary' => 'earning_account',
            'parent' => '0',
          ),
        ),
      ),
      'translatable' => '0',
      'type' => 'taxonomy_term_reference',
    ),
    'field_instance' => array(
      'bundle' => 'income',
      'default_value' => array(
        0 => array(
          'tid' => '9',
        ),
      ),
      'deleted' => '0',
      'description' => '',
      'display' => array(
        'default' => array(
          'label' => 'above',
          'module' => 'taxonomy',
          'settings' => array(),
          'type' => 'taxonomy_term_reference_link',
          'weight' => '12',
        ),
      ),
      'entity_type' => 'node',
      'field_name' => 'field_destination_account',
      'label' => 'Destination account',
      'required' => 0,
      'settings' => array(
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'options',
        'settings' => array(),
        'type' => 'options_buttons',
        'weight' => '7',
      ),
    ),
  );

  // Exported field: 'node-income-field_effective_date'.
  $fields['node-income-field_effective_date'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '1',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'field_effective_date',
      'foreign keys' => array(),
      'indexes' => array(),
      'locked' => '0',
      'module' => 'date',
      'settings' => array(
        'granularity' => array(
          'day' => 'day',
          'hour' => 0,
          'minute' => 0,
          'month' => 'month',
          'second' => 0,
          'year' => 'year',
        ),
        'repeat' => 0,
        'timezone_db' => '',
        'todate' => '',
        'tz_handling' => 'none',
      ),
      'translatable' => '0',
      'type' => 'datetime',
    ),
    'field_instance' => array(
      'bundle' => 'income',
      'deleted' => '0',
      'description' => 'For income, enter the date the funds will be in the bank.
',
      'display' => array(
        'default' => array(
          'label' => 'above',
          'module' => 'editablefields',
          'settings' => array(
            'click_to_edit' => 0,
            'empty_text' => '',
            'fallback_format' => 'date_default',
            'fallback_settings' => array(
              'format_type' => 'short',
              'fromto' => 'both',
              'multiple_from' => '',
              'multiple_number' => '',
              'multiple_to' => '',
              'show_repeat_rule' => 'show',
            ),
          ),
          'type' => 'editable',
          'weight' => '0',
        ),
      ),
      'entity_type' => 'node',
      'field_name' => 'field_effective_date',
      'label' => 'Effective date',
      'required' => 1,
      'settings' => array(
        'default_value' => 'now',
        'default_value2' => 'blank',
        'default_value_code' => '',
        'default_value_code2' => '',
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'date',
        'settings' => array(
          'display_all_day' => 0,
          'increment' => '15',
          'input_format' => 'm/d/Y - H:i:s',
          'input_format_custom' => '',
          'label_position' => 'above',
          'repeat_collapsed' => 0,
          'text_parts' => array(),
          'year_range' => '-3:+3',
        ),
        'type' => 'date_popup',
        'weight' => '0',
      ),
    ),
  );

  // Exported field: 'node-income-field_non_business_payment_total'.
  $fields['node-income-field_non_business_payment_total'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '1',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'field_non_business_payment_total',
      'foreign keys' => array(),
      'indexes' => array(),
      'locked' => '0',
      'module' => 'computed_field',
      'settings' => array(
        'code' => '$entity_field[0][\'value\'] = "";',
        'database' => array(
          'data_default' => '',
          'data_index' => 0,
          'data_length' => '32',
          'data_not_NULL' => 0,
          'data_precision' => '10',
          'data_scale' => '2',
          'data_size' => 'normal',
          'data_type' => 'numeric',
        ),
        'display_format' => '$display_output = $entity_field_item[\'value\'];',
        'store' => 1,
      ),
      'translatable' => '0',
      'type' => 'computed',
    ),
    'field_instance' => array(
      'bundle' => 'income',
      'deleted' => '0',
      'description' => '',
      'display' => array(
        'default' => array(
          'label' => 'above',
          'module' => 'computed_field',
          'settings' => array(),
          'type' => 'computed_field_plain',
          'weight' => 16,
        ),
      ),
      'entity_type' => 'node',
      'field_name' => 'field_non_business_payment_total',
      'label' => 'Non-business payment total',
      'required' => 0,
      'settings' => array(
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 0,
        'module' => 'computed_field',
        'settings' => array(),
        'type' => 'computed',
        'weight' => '17',
      ),
    ),
  );

  // Exported field: 'node-income-field_pending_payment_total'.
  $fields['node-income-field_pending_payment_total'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '1',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'field_pending_payment_total',
      'foreign keys' => array(),
      'indexes' => array(),
      'locked' => '0',
      'module' => 'computed_field',
      'settings' => array(
        'code' => '',
        'database' => array(
          'data_default' => '',
          'data_index' => 0,
          'data_length' => '32',
          'data_not_NULL' => 0,
          'data_precision' => '10',
          'data_scale' => '2',
          'data_size' => 'normal',
          'data_type' => 'numeric',
        ),
        'display_format' => '',
        'store' => 1,
      ),
      'translatable' => '0',
      'type' => 'computed',
    ),
    'field_instance' => array(
      'bundle' => 'income',
      'deleted' => '0',
      'description' => '',
      'display' => array(
        'default' => array(
          'label' => 'above',
          'module' => 'computed_field',
          'settings' => array(),
          'type' => 'computed_field_plain',
          'weight' => '14',
        ),
      ),
      'entity_type' => 'node',
      'field_name' => 'field_pending_payment_total',
      'label' => 'Pending payment total',
      'required' => FALSE,
      'settings' => array(
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'module' => 'computed_field',
        'settings' => array(),
        'type' => 'computed',
        'weight' => '16',
      ),
    ),
  );

  // Exported field: 'node-income-field_r_d'.
  $fields['node-income-field_r_d'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '1',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'field_r_d',
      'foreign keys' => array(),
      'indexes' => array(),
      'locked' => '0',
      'module' => 'computed_field',
      'settings' => array(
        'code' => '$entity_field[0][\'value\'] = "";',
        'database' => array(
          'data_default' => '',
          'data_index' => 0,
          'data_length' => '32',
          'data_not_NULL' => 0,
          'data_precision' => '10',
          'data_scale' => '2',
          'data_size' => 'normal',
          'data_type' => 'numeric',
        ),
        'display_format' => '$display_output = $entity_field_item[\'value\'];',
        'store' => 1,
      ),
      'translatable' => '0',
      'type' => 'computed',
    ),
    'field_instance' => array(
      'bundle' => 'income',
      'deleted' => '0',
      'description' => '',
      'display' => array(
        'default' => array(
          'label' => 'above',
          'module' => 'computed_field',
          'settings' => array(),
          'type' => 'computed_field_plain',
          'weight' => '5',
        ),
      ),
      'entity_type' => 'node',
      'field_name' => 'field_r_d',
      'label' => 'R&D',
      'required' => FALSE,
      'settings' => array(
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'module' => 'computed_field',
        'settings' => array(),
        'type' => 'computed',
        'weight' => '13',
      ),
    ),
  );

  // Exported field: 'node-income-field_r_d_paid'.
  $fields['node-income-field_r_d_paid'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '1',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'field_r_d_paid',
      'foreign keys' => array(),
      'indexes' => array(
        'value' => array(
          0 => 'value',
        ),
      ),
      'locked' => '0',
      'module' => 'list',
      'settings' => array(
        'allowed_values' => array(
          0 => 'No',
          1 => 'Yes',
        ),
        'allowed_values_function' => '',
      ),
      'translatable' => '0',
      'type' => 'list_boolean',
    ),
    'field_instance' => array(
      'bundle' => 'income',
      'default_value' => array(
        0 => array(
          'value' => 0,
        ),
      ),
      'deleted' => '0',
      'description' => '',
      'display' => array(
        'default' => array(
          'label' => 'above',
          'module' => 'editablefields',
          'settings' => array(
            'click_to_edit' => 0,
            'empty_text' => '',
            'fallback_format' => 'list_default',
            'fallback_settings' => array(),
          ),
          'type' => 'editable',
          'weight' => '3',
        ),
      ),
      'entity_type' => 'node',
      'field_name' => 'field_r_d_paid',
      'label' => 'R&D paid',
      'required' => 0,
      'settings' => array(
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'options',
        'settings' => array(
          'display_label' => 1,
        ),
        'type' => 'options_onoff',
        'weight' => '10',
      ),
    ),
  );

  // Exported field: 'node-income-field_r_d_percentage'.
  $fields['node-income-field_r_d_percentage'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '1',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'field_r_d_percentage',
      'foreign keys' => array(
        'node' => array(
          'columns' => array(
            'target_id' => 'nid',
          ),
          'table' => 'node',
        ),
      ),
      'indexes' => array(
        'target_id' => array(
          0 => 'target_id',
        ),
      ),
      'locked' => '0',
      'module' => 'entityreference',
      'settings' => array(
        'handler' => 'base',
        'handler_settings' => array(
          'behaviors' => array(
            'views-select-list' => array(
              'status' => 0,
            ),
          ),
          'sort' => array(
            'direction' => 'DESC',
            'property' => 'created',
            'type' => 'property',
          ),
          'target_bundles' => array(
            'tax_type' => 'tax_type',
          ),
        ),
        'handler_submit' => 'Change handler',
        'target_type' => 'node',
      ),
      'translatable' => '0',
      'type' => 'entityreference',
    ),
    'field_instance' => array(
      'bundle' => 'income',
      'default_value' => NULL,
      'deleted' => '0',
      'description' => '',
      'display' => array(
        'default' => array(
          'label' => 'above',
          'module' => 'entityreference',
          'settings' => array(
            'link' => FALSE,
          ),
          'type' => 'entityreference_label',
          'weight' => '15',
        ),
      ),
      'entity_type' => 'node',
      'field_name' => 'field_r_d_percentage',
      'label' => 'R&D percentage',
      'required' => 0,
      'settings' => array(
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'options',
        'settings' => array(),
        'type' => 'options_buttons',
        'weight' => '4',
      ),
    ),
  );

  // Exported field: 'node-income-field_remaining'.
  $fields['node-income-field_remaining'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '1',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'field_remaining',
      'foreign keys' => array(),
      'indexes' => array(),
      'locked' => '0',
      'module' => 'computed_field',
      'settings' => array(
        'code' => '$entity_field[0][\'value\'] = "";',
        'database' => array(
          'data_default' => '',
          'data_index' => 0,
          'data_length' => '32',
          'data_not_NULL' => 0,
          'data_precision' => '10',
          'data_scale' => '2',
          'data_size' => 'normal',
          'data_type' => 'numeric',
        ),
        'display_format' => '$display_output = $entity_field_item[\'value\'];',
        'store' => 1,
      ),
      'translatable' => '0',
      'type' => 'computed',
    ),
    'field_instance' => array(
      'bundle' => 'income',
      'deleted' => '0',
      'description' => '',
      'display' => array(
        'default' => array(
          'label' => 'above',
          'module' => 'computed_field',
          'settings' => array(),
          'type' => 'computed_field_plain',
          'weight' => '11',
        ),
      ),
      'entity_type' => 'node',
      'field_name' => 'field_remaining',
      'label' => 'Amount remaining',
      'required' => 0,
      'settings' => array(
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 0,
        'module' => 'computed_field',
        'settings' => array(),
        'type' => 'computed',
        'weight' => '15',
      ),
    ),
  );

  // Exported field: 'node-income-field_special'.
  $fields['node-income-field_special'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '1',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'field_special',
      'foreign keys' => array(),
      'indexes' => array(
        'value' => array(
          0 => 'value',
        ),
      ),
      'locked' => '0',
      'module' => 'list',
      'settings' => array(
        'allowed_values' => array(
          0 => 'No',
          1 => 'Yes',
        ),
        'allowed_values_function' => '',
      ),
      'translatable' => '0',
      'type' => 'list_boolean',
    ),
    'field_instance' => array(
      'bundle' => 'income',
      'default_value' => array(
        0 => array(
          'value' => 0,
        ),
      ),
      'deleted' => '0',
      'description' => 'This income should be managed manually, so automated routines and certain calculations should ignore it.',
      'display' => array(
        'default' => array(
          'label' => 'above',
          'module' => 'list',
          'settings' => array(),
          'type' => 'list_default',
          'weight' => '13',
        ),
      ),
      'entity_type' => 'node',
      'field_name' => 'field_special',
      'label' => 'Special',
      'required' => 0,
      'settings' => array(
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'options',
        'settings' => array(
          'display_label' => 1,
        ),
        'type' => 'options_onoff',
        'weight' => '14',
      ),
    ),
  );

  // Exported field: 'node-income-field_tax'.
  $fields['node-income-field_tax'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '1',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'field_tax',
      'foreign keys' => array(),
      'indexes' => array(),
      'locked' => '0',
      'module' => 'computed_field',
      'settings' => array(
        'code' => '$entity_field[0][\'value\'] = "";',
        'database' => array(
          'data_default' => '',
          'data_index' => 0,
          'data_length' => '32',
          'data_not_NULL' => 0,
          'data_precision' => '10',
          'data_scale' => '2',
          'data_size' => 'normal',
          'data_type' => 'numeric',
        ),
        'display_format' => '$display_output = $entity_field_item[\'value\'];',
        'store' => 1,
      ),
      'translatable' => '0',
      'type' => 'computed',
    ),
    'field_instance' => array(
      'bundle' => 'income',
      'deleted' => '0',
      'description' => '',
      'display' => array(
        'default' => array(
          'label' => 'above',
          'module' => 'computed_field',
          'settings' => array(),
          'type' => 'computed_field_plain',
          'weight' => '4',
        ),
      ),
      'entity_type' => 'node',
      'field_name' => 'field_tax',
      'label' => 'Tax',
      'required' => FALSE,
      'settings' => array(
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'module' => 'computed_field',
        'settings' => array(),
        'type' => 'computed',
        'weight' => '12',
      ),
    ),
  );

  // Exported field: 'node-income-field_tax_override'.
  $fields['node-income-field_tax_override'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '1',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'field_tax_override',
      'foreign keys' => array(),
      'indexes' => array(),
      'locked' => '0',
      'module' => 'number',
      'settings' => array(
        'decimal_separator' => '.',
        'precision' => 10,
        'scale' => 2,
      ),
      'translatable' => '0',
      'type' => 'number_decimal',
    ),
    'field_instance' => array(
      'bundle' => 'income',
      'default_value' => NULL,
      'deleted' => '0',
      'description' => 'You don\'t normally have to fill out this field. However, if this line item is income and doesn\'t have tax, you should fill in a 0. If the tax is different than any of the tax rates you can pick, you should really ask the administrator that another tax rate be added. If this is a truly out-of-the-ordinary calculation, you can manually enter the amount of tax here.',
      'display' => array(
        'default' => array(
          'label' => 'above',
          'module' => 'number',
          'settings' => array(
            'decimal_separator' => '.',
            'prefix_suffix' => 1,
            'scale' => '2',
            'thousand_separator' => ',',
          ),
          'type' => 'number_decimal',
          'weight' => '8',
        ),
      ),
      'entity_type' => 'node',
      'field_name' => 'field_tax_override',
      'label' => 'Tax (override)',
      'required' => 0,
      'settings' => array(
        'max' => '',
        'min' => '0',
        'prefix' => '',
        'suffix' => '',
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 0,
        'module' => 'number',
        'settings' => array(),
        'type' => 'number',
        'weight' => '11',
      ),
    ),
  );

  // Exported field: 'node-income-field_tax_paid'.
  $fields['node-income-field_tax_paid'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '1',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'field_tax_paid',
      'foreign keys' => array(),
      'indexes' => array(
        'value' => array(
          0 => 'value',
        ),
      ),
      'locked' => '0',
      'module' => 'list',
      'settings' => array(
        'allowed_values' => array(
          0 => 'No',
          1 => 'Yes',
        ),
        'allowed_values_function' => '',
      ),
      'translatable' => '0',
      'type' => 'list_boolean',
    ),
    'field_instance' => array(
      'bundle' => 'income',
      'default_value' => array(
        0 => array(
          'value' => 0,
        ),
      ),
      'deleted' => '0',
      'description' => 'If the tax connected to this line item has been paid, check this box.',
      'display' => array(
        'default' => array(
          'label' => 'above',
          'module' => 'editablefields',
          'settings' => array(
            'click_to_edit' => FALSE,
            'empty_text' => '',
            'fallback_format' => NULL,
            'fallback_settings' => array(),
          ),
          'type' => 'editable',
          'weight' => '2',
        ),
      ),
      'entity_type' => 'node',
      'field_name' => 'field_tax_paid',
      'label' => 'Tax paid',
      'required' => 0,
      'settings' => array(
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'options',
        'settings' => array(
          'display_label' => 1,
        ),
        'type' => 'options_onoff',
        'weight' => '9',
      ),
    ),
  );

  // Exported field: 'node-income-field_tax_type'.
  $fields['node-income-field_tax_type'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '1',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'field_tax_type',
      'foreign keys' => array(),
      'indexes' => array(
        'target_entity' => array(
          0 => 'target_id',
        ),
        'target_id' => array(
          0 => 'target_id',
        ),
      ),
      'locked' => '0',
      'module' => 'entityreference',
      'settings' => array(
        'handler' => 'base',
        'handler_settings' => array(
          'behaviors' => array(
            'views-select-list' => array(
              'status' => 0,
            ),
          ),
          'sort' => array(
            'direction' => 'DESC',
            'property' => 'created',
            'type' => 'property',
          ),
          'target_bundles' => array(
            'tax_type' => 'tax_type',
          ),
        ),
        'handler_submit' => 'Change handler',
        'target_type' => 'node',
      ),
      'translatable' => '0',
      'type' => 'entityreference',
    ),
    'field_instance' => array(
      'bundle' => 'income',
      'default_value' => NULL,
      'deleted' => '0',
      'description' => 'Select the tax that applies to this <em>income</em>.',
      'display' => array(
        'default' => array(
          'label' => 'above',
          'module' => 'entityreference',
          'settings' => array(
            'link' => TRUE,
          ),
          'type' => 'entityreference_label',
          'weight' => '10',
        ),
      ),
      'entity_type' => 'node',
      'field_name' => 'field_tax_type',
      'label' => 'Tax type',
      'required' => 0,
      'settings' => array(
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'options',
        'settings' => array(),
        'type' => 'options_buttons',
        'weight' => '3',
      ),
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Amount');
  t('Amount remaining');
  t('Amount transferred');
  t('Business-related');
  t('Comment');
  t('Destination account');
  t('Effective date');
  t('Enter anything that needs to be known about this line item.');
  t('Enter the amount of income.');
  t('Enter the amount transferred and usable for payments.');
  t('For income, enter the date the funds will be in the bank.
');
  t('If the tax connected to this line item has been paid, check this box.');
  t('If this <em>income</em> came from business-related activities, check this box.');
  t('Non-business payment total');
  t('Notes');
  t('Pending payment total');
  t('R&D');
  t('R&D paid');
  t('R&D percentage');
  t('Select the tax that applies to this <em>income</em>.');
  t('Special');
  t('Tax');
  t('Tax (override)');
  t('Tax paid');
  t('Tax type');
  t('This income should be managed manually, so automated routines and certain calculations should ignore it.');
  t('You don\'t normally have to fill out this field. However, if this line item is income and doesn\'t have tax, you should fill in a 0. If the tax is different than any of the tax rates you can pick, you should really ask the administrator that another tax rate be added. If this is a truly out-of-the-ordinary calculation, you can manually enter the amount of tax here.');

  return $fields;
}
