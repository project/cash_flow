<?php
/**
 * @file
 * cash_flow_deluxe.features.inc
 */

/**
 * Implements hook_views_api().
 */
function cash_flow_deluxe_views_api() {
  return array("api" => "3.0");
}
